cd Downloads

## A
mkdir forensic_log_website_daffainfo_log
cp -r log_website_daffainfo.log /. ~/Downloads/forensic_log_website_daffainfo_log
cd forensic_log_website_daffainfo_log


cat > soal2_forensic.dapos.sh
echo 'SISOP || TC 2020'
chmod u+x soal2_forensic.dapos.sh


## B
awk -F: '{ NR>1; sum += $3; arr[$3]++ } END { for(i in arr){ req++; sum+=arr[i];} print("Rata-rata serangan adalah sebanyak:\n", sum/req, " per jam") }'  log_website_daffainfo.log > ratarata.txt


## C
awk -F: 'BEGIN{ NR>1; max=0}{if ($1>!+max) max=$1} END{print "IP yang paling banyak mengakses server adalah: \n", $1}' log_website_daffainfo.log > result.txt


## D
awk -F: 'BEGIN{$6 = "curl"; max=0;} { if (max = $1 + 1){ max = $3 } } END{ print "Ada :", max, "\n users yang menggunakan curl sebagai username" }' log_website_daffainfo.log > result2.txt

## E
awk -F '22/Jan/2022:02:' '{print $2}' log_website_daffainfo.log > result3.txt

bash soal2_forensic_dapos.sh
