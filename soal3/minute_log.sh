#!/bin/bash
logfolder="/home/$USER/log"
mkdir -p $logfolder
logfile="$logfolder/metrics_`date +%Y%m%d%H%M%S`.log"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $logfile
free -m | awk 'NR==2{printf $2 "," $3 "," $4 "," $5 "," $6 "," $7} NR==3{ printf $2 "," $3 "," $4 "," $HOME }' >> $logfile
du -sh $HOME | awk '{printf $2 "," $1 "\n"}' >> $logfile
