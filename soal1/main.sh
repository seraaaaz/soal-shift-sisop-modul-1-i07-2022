#!/bin/bash

function_zip() {
    zip --pass $pass -r $folder.zip $folder
    rm -rf $folder
}

function_unzip() {
    unzip -P $pass $folder.zip
}

function_download() {
    for (( i = 1; i <= n; i++))
    do 
    if [ $i -le 9 ]
    then 
    nol="0"
    else
    nol=""
    fi

    wget -O $folder/PIC_$i.jpg https://loremflickr.com/320/240 
    done
    function_zip
}


echo "Login Attempt"
echo -n "username: "
read uname;
echo -n "password: "
read pass;

location="users/user.txt"

if grep -qF "$pass" $location
then
echo "LOGIN: INFO User $uname logged in" >> log.txt
echo "choose dl/att"
read choose;

if [ "$choose" == "dl" ];
then
echo "amount of pictures"
read n;

folder=$(date +%Y-%m-%d)_$uname

if [[ ! -f "$folder.zip" ]]
then
mkdir $folder
function_download
else
function_unzip
fi
# count=1

elif [ "$choose" == "att" ]
then
awk ' BEGIN { print "Counting!!" }
/logged/{login++} /Failed/{fail++}
END { print "Success:", login, "Failed:", fail }' log.txt
fi
else
echo "LOGIN: ERROR Failed login attempt on User $uname" >> log.txt
fi
